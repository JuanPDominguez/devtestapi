using Microsoft.EntityFrameworkCore;

namespace DevTest
{
    public class BetContext: DbContext, IBetContext
    {
        public BetContext(DbContextOptions<BetContext> options): base(options)
        {
            
        }
        public DbSet <Bet> Bets { get; init; }
        public DbSet <Game> Games { get; init; }
        public DbSet <User> Users { get; init; }
    }
}