using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DevTest
{
    public interface IBetContext
    {
        DbSet<Bet> Bets { get; init; }
        DbSet <Game> Games { get; init; }
        DbSet <User> Users { get; init; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}