using System;
using System.ComponentModel.DataAnnotations;

namespace DevTest
{
    /// <summary>
    /// User is a person with ID and name. This person will bet money here for a specific team in different games
    /// </summary>
    public class User
    {
        [Key]
        public Guid UserId { get; set; }
        [Required]
        public string UserName { get; set; }
    }
}
  