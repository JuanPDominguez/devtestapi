using System;
using System.ComponentModel.DataAnnotations;

namespace DevTest
{
    /// <summary>
    /// Game is a class to model a specific game between teams. These games has a Score too
    /// </summary>
    public class Game
    {
        [Key]
        public Guid GameId { get; set; }
        [Required]
        public string GameHostTeam { get; set; }
        [Required]
        public string GameGuestTeam { get; set; }
        [Required]
        public int GameLocalTeamScore { get; set; }
        [Required]
        public int GameGuestTeamScore { get; set; }
        [Required]
        public DateTime GameDate { get; set; }
    }
}