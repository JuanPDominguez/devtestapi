using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DevTest
{
    /// <summary>
    /// Bet is a class to model the bets done for the users to a specific game 
    /// </summary>
    public class Bet
    {
        [Key]
        public Guid BetId { get; set; }
        [ForeignKey("Game")]
        public Guid GameId { get; set; }
        [ForeignKey("User")]
        public Guid UserId { get; set; }
        [Required]
        public int BetAmount { get; set; }
        [Required]
        public int GameLocalTeamScore { get; set; }
        [Required]
        public int GameGuestTeamScore { get; set; }
        [Required]
        public bool BetCompleted { get; set; }

        public Game Games { get; set; }
        public User Users { get; set; }
    }
}