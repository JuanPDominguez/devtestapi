using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DevTest.Repositories
{
    public interface IBetRepository
    {
        Task<Bet> GetBet(Guid id);
        Task<IEnumerable<Bet>> GetBets();
        Task AddBet(Bet bet);
        Task DeleteBet(Guid id);
        Task EditBet(Bet bet);
    }
}