using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DevTest;
using Microsoft.EntityFrameworkCore;

namespace DevTest.Repositories
{
    public class GameRepository: IGameRepository
    {
        private readonly IBetContext _gameContext;
        public GameRepository(IBetContext gameContext)
        {
            _gameContext = gameContext;
        }
        public async Task<Game> AddGame(Game game)
        {
            _gameContext.Games.Add(game);
            await _gameContext.SaveChangesAsync(); 
            return game;
        }
        public async Task<IEnumerable<Game>> GetGames()
        {
           return await _gameContext.Games.ToListAsync();
        }
        public async Task<Game> GetGame(Guid gameId) 
        {
            return await _gameContext.Games.FindAsync(gameId); 
        }
        public async Task<Game> EditGame(Game game)
        {
            var toUpdate = await _gameContext.Games.FindAsync(game.GameId);
            if (toUpdate == null)
                throw new NullReferenceException();
            
            toUpdate.GameGuestTeamScore = game.GameGuestTeamScore;
            toUpdate.GameLocalTeamScore = game.GameLocalTeamScore;
            await _gameContext.SaveChangesAsync();
            return game;
        }
        public async Task DeleteGame(Guid GameId)
        {
            var toDelete = await _gameContext.Games.FindAsync(GameId);
            Console.WriteLine(toDelete);
            if(toDelete == null)
                throw new NullReferenceException();

            _gameContext.Games.Remove(toDelete);
            await _gameContext.SaveChangesAsync();
        }
    }
}