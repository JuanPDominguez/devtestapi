using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevTest;
using Microsoft.EntityFrameworkCore;

namespace DevTest.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly IBetContext _userContext;
        public UserRepository(IBetContext userContext)
        {
            _userContext = userContext;
        }
        public async Task<User> AddUser(User user)
        {
            _userContext.Users.Add(user);
            await _userContext.SaveChangesAsync(); 
            return user;
        }
        public async Task<IEnumerable<User>> GetUsers()
        {
           return await _userContext.Users.ToListAsync();
        }
        public async Task<User> GetUser(Guid userId) 
        {
            return await _userContext.Users.FindAsync(userId); 
        }
        public async Task<User> EditUser(User user)
        {
            var toUpdate = await _userContext.Users.FindAsync(user.UserId);
            if (toUpdate == null)
                throw new NullReferenceException();
            
            toUpdate.UserName = user.UserName;
            await _userContext.SaveChangesAsync();
            return user;
        }
        public async Task DeleteUser(Guid userId)
        {
            var toDelete = await _userContext.Users.FindAsync(userId);
            Console.WriteLine(toDelete);
            if(toDelete == null)
                throw new NullReferenceException();

            _userContext.Users.Remove(toDelete);
            await _userContext.SaveChangesAsync();
        }
    }
}