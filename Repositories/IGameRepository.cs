using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevTest;

namespace DevTest.Repositories
{
    public interface IGameRepository
    {
        Task<Game> AddGame(Game game);
        Task<IEnumerable<Game>> GetGames();
        Task<Game> GetGame(Guid gameId);
        Task<Game> EditGame(Game game);
        Task DeleteGame(Guid gameId);
    }
}