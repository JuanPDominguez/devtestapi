using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using DevTest.Repositories;
using System.Threading.Tasks;

namespace DevTest.Controllers
{
    [ApiController]
    [Route("api/bet")]
    
    public class BetController : ControllerBase
    {
       private IBetRepository _betRepository;

       public BetController(IBetRepository betRepository)
       {
           _betRepository = betRepository;
       }

        [HttpGet("{betId}")]
        public async Task<ActionResult<Bet>> GetBetByIdAsync(Guid betId)
        {
            var bet = await _betRepository.GetBet(betId);
            if(bet == null)
                return NotFound();
            return Ok(bet);
        }

        //Create
        [HttpPost]
        public async Task<IActionResult> NewBetAsync(Bet bet)
        {
           await _betRepository.AddBet(bet);
           return Ok($"Bet created");
        }
        //Research
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Bet>>> GetBetsAsync()
        {
            var bets = await _betRepository.GetBets();
            return Ok(bets);
        }
        //Update
        [HttpPut]
        public async Task<IActionResult> EditBetAsync(Bet bet)
        {
            Bet betFound = await _betRepository.GetBet(bet.BetId);
            if(betFound != null)
            {
                bet.BetId = betFound.BetId;
                await _betRepository.EditBet(bet);
            }
            return Ok($"Bet with ID {bet.BetId} has the amount {bet.BetAmount} now!" );
        }
        //Delete
        [HttpDelete("{betId}")]
        public async Task<IActionResult> DeleteBetAsync(Guid betId)
        {
            await _betRepository.DeleteBet(betId);
            return Ok("Bet Deleted");
        }
    }
}